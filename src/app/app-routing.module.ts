import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './componentes/about/about.component';
import { ContactComponent } from './componentes/contact/contact.component';
import { ErrorComponentComponent } from './componentes/error-component/error-component.component';
import { FaqsComponentComponent } from './componentes/faqs-component/faqs-component.component';
import { HomeComponent } from './componentes/home/home.component';
import { PartnersComponent } from './componentes/partners/partners.component';
import { ServicesComponentComponent } from './componentes/services-component/services-component.component';
import { TeamComponentComponent } from './componentes/team-component/team-component.component';



const routes: Routes = [
  {path:'', component: HomeComponent},
  {path:'about', component: AboutComponent},
  {path:'services-component',component:ServicesComponentComponent},
  {path:'faqs-component', component:FaqsComponentComponent},
  {path:'error-component', component: ErrorComponentComponent},
  {path:'team-component', component:TeamComponentComponent},
  {path:'contact', component: ContactComponent},
  {path:'partners', component:PartnersComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
