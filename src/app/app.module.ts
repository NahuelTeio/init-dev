import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module';
import { HomeComponent } from './componentes/home/home.component';
import { AboutComponent } from './componentes/about/about.component';
import { ServicesComponentComponent } from './componentes/services-component/services-component.component';
import { FaqsComponentComponent } from './componentes/faqs-component/faqs-component.component';
import { ErrorComponentComponent } from './componentes/error-component/error-component.component';
import { ContactComponent } from './componentes/contact/contact.component';
import { TeamComponentComponent } from './componentes/team-component/team-component.component';
import { PartnersComponent } from './componentes/partners/partners.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AboutComponent,
    ServicesComponentComponent,
    FaqsComponentComponent,
    ErrorComponentComponent,
    ContactComponent,
    TeamComponentComponent,
    PartnersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
